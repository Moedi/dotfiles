## Profile Path: /home/<username>/.config/powershell/Microsoft.PowerShell_profile.ps1

Function Get-PubIP {
    (Invoke-WebRequest http://ifconfig.me/ip ).Content
}

Function va {
    Get-ChildItem activate.ps1 -Recurse -Depth 2 | ForEach-Object { $_.FullName } | Invoke-Expression
}

Function ve {
    "python3 -m venv ./venv"  | Invoke-Expression
}

Import-Module -Name PSReadLine

# Adds unix shell functionality
Set-PSReadLineOption -EditMode Windows
Set-PSReadLineOption -PredictionSource History
Set-PSReadLineKeyHandler -Key "Ctrl+f" -ScriptBlock {
    [Microsoft.PowerShell.PSConsoleReadLine]::AcceptSuggestion()
    [Microsoft.PowerShell.PSConsoleReadLine]::EndOfLine()
}
Set-PSReadLineKeyHandler -Key "Ctrl+k" -Function ForwardDeleteLine
Set-PSReadLineKeyHandler -Key "Ctrl+u" -Function BackwardDeleteLine
Set-PSReadLineKeyHandler -Key "Ctrl+w" -Function BackwardDeleteWord

# Loads my oh-my-posh theme
$env:VIRTUAL_ENV_DISABLE_PROMPT = 1
$currentUser = whoami
oh-my-posh init pwsh --config "/home/$currentUser/.config/oh-my-posh/themes/mytheme.omp.json" | Invoke-Expression